var config = require('./config'),
    async = require('async'),
    gpio = require('rpi-gpio'),
    sequence = require('./SongSequence'),
    seqLength = sequence.length,
    on = false;

main();

/* MAIN CONTROLLER */
function main() {
    async.series([
   function (callback) {
            setup();
            timeout(1000, callback);
   },
   function (callback) { 
           	checkTime();
   }
 ]);
}

/* FUNCTIONS */
function readSequence(len) {
	console.log("len = "+ len)
    if (len < seqLength) {
        var seq = sequence[len];
        console.log(seq);
		write(seq.relay, seq.pow);
        if (seq.dur > 0){
       	 timeout(seq.dur, function(){
        	return readSequence(len += 1);
       	 });
        }else{
        	readSequence(len += 1);
        }
    }else{
    	checkTime();
    }
}

function timeout(dur, func) {
    console.log("timeout out for " + dur);
    setTimeout(function () {
        func();
    }, dur);
}

function checkTime() {
	var date = new Date();
    var current_hour = date.getHours();
    console.log("current time = " + current_hour);
    if (current_hour > 22 || current_hour < 16) {// between 4 and 10 PM
    	if (on){
    		closePins();
    	}
        console.log("sleeping... current hour is " + current_hour);
        timeout(600000, checkTime);
    } else {
    	console.log("It's time!");
    	on = true;
        readSequence(0);
    }
}

function setup() {
	console.log("setting up");
    gpio.setup(config.RELAY_1, gpio.DIR_OUT);
    gpio.setup(config.RELAY_2, gpio.DIR_OUT);
    gpio.setup(config.RELAY_3, gpio.DIR_OUT);
    gpio.setup(config.RELAY_4, gpio.DIR_OUT);
    gpio.setup(config.RELAY_5, gpio.DIR_OUT);
    gpio.setup(config.RELAY_6, gpio.DIR_OUT);
    gpio.setup(config.RELAY_7, gpio.DIR_OUT);
    gpio.setup(config.RELAY_8, gpio.DIR_OUT);
}

function write(pinNum, pow) {
    // write to pin
    console.log("attempting to write " + pinNum);
    gpio.write(pinNum, pow, function (err) {
        if (err) throw err;
        console.log('Written to pin');
    });
}

function closePins() {
    console.log("closing pins");
    gpio.write(config.RELAY_1, config.RELAY_OFF);
    gpio.write(config.RELAY_2, config.RELAY_OFF);
    gpio.write(config.RELAY_3, config.RELAY_OFF);
    gpio.write(config.RELAY_4, config.RELAY_OFF);
    gpio.write(config.RELAY_5, config.RELAY_OFF);
    gpio.write(config.RELAY_6, config.RELAY_OFF);
    gpio.write(config.RELAY_7, config.RELAY_OFF);
    gpio.write(config.RELAY_8, config.RELAY_OFF);
    gpio.destroy(function () {
        console.log('Closed pins, now exit');
    });
    on = false;
}

// Start reading from stdin so we don't exit.
process.stdin.resume();
process.on('SIGINT', function () {
    console.log('EXITING');
    async.series([
            function (callback) {
            closePins();
            timeout(3000, callback);
            },
            function (callback) {
            console.log("process exit");
            process.exit();
            }
        ]);
});