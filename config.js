var config = {};

config.RELAY_1 = 22;
config.RELAY_2 = 11;
config.RELAY_3 = 12;
config.RELAY_4 = 13;
config.RELAY_5 = 15;
config.RELAY_6 = 16;
config.RELAY_7 = 18;
config.RELAY_8 = 7;

config.RELAY_ON = 1;
config.RELAY_OFF = 0;
config.RELAY_TIMEOUT = 500;

module.exports = config;