// relay = relay number to trigger
// dur = duration until next sequence
// pow = is the light powered on/off?  1 = on, 0 = off

var config = require('./config');

var sequence = [
     {
         "relay": config.RELAY_1,
         "dur": 5000,
         "pow": 1
     },
     {
         "relay": config.RELAY_2,
         "dur": 5000,
         "pow": 1
     },
     {
         "relay": config.RELAY_3,
         "dur": 5000,
         "pow": 1
     },
     {
         "relay": config.RELAY_4,
         "dur": 5000,
         "pow": 1
     },
     {
         "relay": config.RELAY_5,
         "dur": 5000,
         "pow": 1
     },
     {
         "relay": config.RELAY_6,
         "dur": 5000,
         "pow": 1
     },
     {
         "relay": config.RELAY_7,
         "dur": 5000,
         "pow": 1
     },
//      {
//          "relay": config.RELAY_8,
//          "dur": 5000,
//          "pow": 1
//      },
     {
         "relay": config.RELAY_1,
         "dur": 0,
         "pow": 0
     },
     {
         "relay": config.RELAY_2,
         "dur": 0,
         "pow": 0
     },
     {
         "relay": config.RELAY_3,
         "dur": 0,
         "pow": 0
     },
     {
         "relay": config.RELAY_4,
         "dur": 0,
         "pow": 0
     },
     {
         "relay": config.RELAY_5,
         "dur": 0,
         "pow": 0
     },
     {
         "relay": config.RELAY_6,
         "dur": 0,
         "pow": 0
     },
     {
         "relay": config.RELAY_7,
         "dur": 2000,
         "pow": 0
     },
//      {
//          "relay": config.RELAY_8,
//          "dur": 5000,
//          "pow": 0
//      },
    {
        "relay": config.RELAY_1,
        "dur": 0,
        "pow": 1
    },
    {
        "relay": config.RELAY_2,
        "dur": 0,
        "pow": 1
    },
    {
        "relay": config.RELAY_3,
        "dur": 0,
        "pow": 1
    },
    {
        "relay": config.RELAY_4,
        "dur": 0,
        "pow": 1
    },
    {
        "relay": config.RELAY_5,
        "dur": 0,
        "pow": 1
    },
    {
        "relay": config.RELAY_6,
        "dur": 0,
        "pow": 1
    },
    {
        "relay": config.RELAY_7,
        "dur": 2000,
        "pow": 1
    },
//      {
//          "relay": config.RELAY_8,
//          "dur": 5000,
//          "pow": 0
//      },
    {
        "relay": config.RELAY_1,
        "dur": 0,
        "pow": 0
    },
    {
        "relay": config.RELAY_2,
        "dur": 0,
        "pow": 0
    },
    {
        "relay": config.RELAY_3,
        "dur": 0,
        "pow": 0
    },
    {
        "relay": config.RELAY_4,
        "dur": 0,
        "pow": 0
    },
    {
        "relay": config.RELAY_5,
        "dur": 0,
        "pow": 0
    },
    {
        "relay": config.RELAY_6,
        "dur": 0,
        "pow": 0
    },
    {
        "relay": config.RELAY_7,
        "dur": 2000,
        "pow": 0
    },
//      {
//          "relay": config.RELAY_8,
//          "dur": 5000,
//          "pow": 0
//      },
    {
        "relay": config.RELAY_1,
        "dur": 0,
        "pow": 1
    },
    {
        "relay": config.RELAY_2,
        "dur": 0,
        "pow": 1
    },
    {
        "relay": config.RELAY_3,
        "dur": 0,
        "pow": 1
    },
    {
        "relay": config.RELAY_4,
        "dur": 0,
        "pow": 1
    },
    {
        "relay": config.RELAY_5,
        "dur": 0,
        "pow": 1
    },
    {
        "relay": config.RELAY_6,
        "dur": 0,
        "pow": 1
    },
    {
        "relay": config.RELAY_7,
        "dur": 2000,
        "pow": 1
    },
//      {
//          "relay": config.RELAY_8,
//          "dur": 5000,
//          "pow": 0
//      },
   {
        "relay": config.RELAY_1,
        "dur": 0,
        "pow": 0
    },
    {
        "relay": config.RELAY_2,
        "dur": 0,
        "pow": 0
    },
    {
        "relay": config.RELAY_3,
        "dur": 0,
        "pow": 0
    },
    {
        "relay": config.RELAY_4,
        "dur": 0,
        "pow": 0
    },
    {
        "relay": config.RELAY_5,
        "dur": 0,
        "pow": 0
    },
    {
        "relay": config.RELAY_6,
        "dur": 0,
        "pow": 0
    },
    {
        "relay": config.RELAY_7,
        "dur": 2000,
        "pow": 0
    },
//      {
//          "relay": config.RELAY_8,
//          "dur": 5000,
//          "pow": 0
//      },
    {
        "relay": config.RELAY_1,
        "dur": 0,
        "pow": 1
    },
    {
        "relay": config.RELAY_2,
        "dur": 0,
        "pow": 1
    },
    {
        "relay": config.RELAY_3,
        "dur": 0,
        "pow": 1
    },
    {
        "relay": config.RELAY_4,
        "dur": 0,
        "pow": 1
    },
    {
        "relay": config.RELAY_5,
        "dur": 0,
        "pow": 1
    },
    {
        "relay": config.RELAY_6,
        "dur": 0,
        "pow": 1
    },
    {
        "relay": config.RELAY_7,
        "dur": 100000,
        "pow": 1
    },
//      {
//          "relay": config.RELAY_8,
//          "dur": 5000,
//          "pow": 0
//      },
  {
        "relay": config.RELAY_1,
        "dur": 0,
        "pow": 0
    },
    {
        "relay": config.RELAY_2,
        "dur": 0,
        "pow": 0
    },
    {
        "relay": config.RELAY_3,
        "dur": 0,
        "pow": 0
    },
    {
        "relay": config.RELAY_4,
        "dur": 0,
        "pow": 0
    },
    {
        "relay": config.RELAY_5,
        "dur": 0,
        "pow": 0
    },
    {
        "relay": config.RELAY_6,
        "dur": 0,
        "pow": 0
    },
    {
        "relay": config.RELAY_7,
        "dur": 200,
        "pow": 0
    },
//      {
//          "relay": config.RELAY_8,
//          "dur": 5000,
//          "pow": 0
//      },
    {
        "relay": config.RELAY_1,
        "dur": 200,
        "pow": 1
    },
        {
        "relay": config.RELAY_1,
        "dur": 200,
        "pow": 0
    },
    {
        "relay": config.RELAY_2,
        "dur": 200,
        "pow": 1
    },
    {
        "relay": config.RELAY_2,
        "dur": 200,
        "pow": 0
    },
    {
        "relay": config.RELAY_3,
        "dur": 200,
        "pow": 1
    },
    {
        "relay": config.RELAY_3,
        "dur": 200,
        "pow": 0
    },
    {
        "relay": config.RELAY_4,
        "dur": 200,
        "pow": 1
    },
    {
        "relay": config.RELAY_4,
        "dur": 200,
        "pow": 0
    },
    {
        "relay": config.RELAY_5,
        "dur": 200,
        "pow": 1
    },
    {
        "relay": config.RELAY_5,
        "dur": 200,
        "pow": 0
    },
    {
        "relay": config.RELAY_6,
        "dur": 200,
        "pow": 1
    },
    {
        "relay": config.RELAY_6,
        "dur": 200,
        "pow": 0
    },
    {
        "relay": config.RELAY_7,
        "dur": 200,
        "pow": 1
    },
    {
        "relay": config.RELAY_7,
        "dur": 200,
        "pow": 0
    },
	 {
        "relay": config.RELAY_6,
        "dur": 200,
        "pow": 1
    },
    {
        "relay": config.RELAY_6,
        "dur": 200,
        "pow": 0
    },
	    {
        "relay": config.RELAY_5,
        "dur": 200,
        "pow": 1
    },
    {
        "relay": config.RELAY_5,
        "dur": 200,
        "pow": 0
    },
	    {
        "relay": config.RELAY_4,
        "dur": 200,
        "pow": 1
    },
    {
        "relay": config.RELAY_4,
        "dur": 200,
        "pow": 0
    },
	    {
        "relay": config.RELAY_3,
        "dur": 200,
        "pow": 1
    },
    {
        "relay": config.RELAY_3,
        "dur": 200,
        "pow": 0
    },
        {
        "relay": config.RELAY_2,
        "dur": 200,
        "pow": 1
    },
    {
        "relay": config.RELAY_2,
        "dur": 200,
        "pow": 0
    },
    {
        "relay": config.RELAY_1,
        "dur": 200,
        "pow": 1
    },
        {
        "relay": config.RELAY_1,
        "dur": 200,
        "pow": 0
    },
    {
        "relay": config.RELAY_1,
        "dur": 0,
        "pow": 1
    },
    {
        "relay": config.RELAY_3,
        "dur": 0,
        "pow": 1
    },
    {
        "relay": config.RELAY_5,
        "dur": 0,
        "pow": 1
    },
    {
        "relay": config.RELAY_7,
        "dur": 1000,
        "pow": 1
    },
       {
        "relay": config.RELAY_1,
        "dur": 0,
        "pow": 0
    },
    {
        "relay": config.RELAY_3,
        "dur": 0,
        "pow": 0
    },
    {
        "relay": config.RELAY_5,
        "dur": 0,
        "pow": 0
    },
    {
        "relay": config.RELAY_7,
        "dur": 0,
        "pow": 0
    },
        {
        "relay": config.RELAY_2,
        "dur": 0,
        "pow": 1
    },
    {
        "relay": config.RELAY_4,
        "dur": 0,
        "pow": 1
    },
    {
        "relay": config.RELAY_6,
        "dur": 1000,
        "pow": 1
    },



];

module.exports = sequence;